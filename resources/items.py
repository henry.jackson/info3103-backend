from flask import jsonify, make_response, request, abort
from flask_restful import Resource, reqparse

from db import get_items, create_item, get_item, delete_item, update_item

parser = reqparse.RequestParser()
parser.add_argument('name', type=str, required=True)
parser.add_argument('description', type=str, required=True)

import uuid

class Items(Resource):
    @staticmethod
    def get(wish_id):
        return make_response(jsonify(get_items(wish_id)), 200)

    @staticmethod
    def post(user_id,wish_id):
        args = parser.parse_args() 
        create_item(wish_id, args['name'], args['description'])
        return make_response(jsonify({}), 201)


class Item(Resource):
    @staticmethod
    def get(item_id):
        rows = get_wish(item_id)
        if not rows:
            abort(404, "Not Found")
        return make_response(jsonify(rows[0]), 200)

    @staticmethod
    def delete(item_id):
        delete_item(item_id)
        return make_response(jsonify({}), 204)

    @staticmethod
    def put(item_id):
        args = parser.parse_args()
        update_item(item_id, args['name'], args['description'])
        return make_response(jsonify({}), 204)
