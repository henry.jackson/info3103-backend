from pymysql.err import IntegrityError
from flask import jsonify, make_response, request, abort
from flask_restful import Resource

from db import get_users, create_user, get_user, delete_user, update_user


class Users(Resource):
    @staticmethod
    def get():
        return make_response(jsonify(get_users()), 200)

    @staticmethod
    def post():
        try:
            create_user(request.json['username'])
        except IntegrityError:
            abort(400, "Bad Request")
        except KeyError:
            abort(400, "Bad Request")
        else:
            return make_response(jsonify({}), 201)


class User(Resource):
    @staticmethod
    def get(user_id):
        rows = get_user(user_id)
        if not rows:
            abort(404, "Not Found")
        return make_response(jsonify(rows[0]), 200)
