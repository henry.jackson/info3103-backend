from flask import make_response, jsonify, session, abort

from db import get_user, get_users


def login_required(func):

    def wrapper(*args, **kwargs):
        if 'username' not in session:
            abort(401)
        return func(*args, **kwargs)

    return wrapper


@login_required
def check_authorized(owner_id):
    rows = get_users()
    for row in rows:
        if row['id'] == owner_id:
            if session['username'] == row['username']:
                return
    abort(403, "Forbidden")
