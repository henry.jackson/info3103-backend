#!/usr/bin/env python3

import pymysql.cursors
import settings


CREATE_TABLE_USERS = """
CREATE TABLE users(
    id int not null auto_increment,
    username char(30) not null unique,
    primary key(id)
    );
"""

CREATE_TABLE_WISHES = """
CREATE TABLE wishes (
    id INT NOT NULL auto_increment,
    user_id INT NOT NULL,
    wish_name char(100) NOT NULL,
    primary key(id),
    FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE
    );
"""

CREATE_TABLE_ITEMS = """
CREATE TABLE items (
    id int not null auto_increment,
    wish_id int not null,
    item_name char(100) not null,
    item_description char(255) not null,
    primary key(id)
    );
"""

INSERT_WISH = """
DROP PROCEDURE IF EXISTS create_wish;
CREATE PROCEDURE create_wish(
        IN _wish_name CHAR(100),
        IN _user_id INT
)
    INSERT INTO wishes (user_id,wish_name) VALUES (_user_id,_wish_name);
"""

GET_WISHES = """
DROP PROCEDURE IF EXISTS get_wishes;
CREATE PROCEDURE get_wishes(
    IN _user_id INT
)
    SELECT * FROM wishes WHERE user_id=_user_id;
"""

UPDATE_WISH_NAME = """
DROP PROCEDURE IF EXISTS update_wish_name;
CREATE PROCEDURE update_wish_name(
        IN _id INT,
        IN _name CHAR(100)
    )
    UPDATE wishes SET wish_name=_name WHERE id=_id;
"""
DELETE_WISH_ITEM = """
DROP PROCEDURE IF EXISTS delete_wish_item;
CREATE PROCEDURE delete_wish_item(
        IN _id INT,
        IN _item_id CHAR(100)
    )
    DELETE FROM wishes WHERE id=_id AND item_id=_item_id;
"""

GET_WISH = """
DROP PROCEDURE IF EXISTS get_wish;
CREATE PROCEDURE get_wish(
        IN _id INT
    )
    SELECT * FROM wishes WHERE id=_id;
"""

DELETE_WISH = """
DROP PROCEDURE IF EXISTS delete_wish;
CREATE PROCEDURE delete_wish(
        IN _id INT
    )
    DELETE FROM wishes WHERE id=_id;
"""

INSERT_USER = """
DROP PROCEDURE IF EXISTS create_user;
CREATE PROCEDURE create_user(
        IN _username CHAR(30)
    )
    INSERT INTO users (username) VALUES (_username);
"""

GET_USERS = """
DROP PROCEDURE IF EXISTS get_users;
CREATE PROCEDURE get_users()
    SELECT * FROM users;
"""

GET_USER = """
DROP PROCEDURE IF EXISTS get_user;
CREATE PROCEDURE get_user(
        IN _id INT
    )
    SELECT * FROM users WHERE id=_id;
"""

DELETE_USER = """
DROP PROCEDURE IF EXISTS delete_user;
CREATE PROCEDURE delete_user(
        IN _id INT
    )
    DELETE FROM users WHERE id=_id;
"""

UPDATE_USER = """
DROP PROCEDURE IF EXISTS update_user;
CREATE PROCEDURE update_user(
        IN _id INT,
        IN _username CHAR(30)
    )
 UPDATE users SET username=_username WHERE id=_id;
"""

INSERT_ITEM = """
DROP PROCEDURE IF EXISTS create_item;
CREATE PROCEDURE create_item(
        IN _item_id CHAR(100),
        IN _item_name CHAR(100),
        IN _item_description CHAR(255),
        IN _wish_ID CHAR(100)
    )
    INSERT INTO items (item_name, item_description, wish_id) VALUES (_item_id,_item_name, _item_description,_wish_id)
"""
UPDATE_ITEM = """
DROP PROCEDURE IF EXISTS update_item;
CREATE PROCEDURE update_item(
        IN _id INT,
        IN _name CHAR(100),
        IN _description CHAR(255)
    )
    UPDATE wishes SET item_name=_name, item_description=_description WHERE id=_id;
"""

GET_ITEMS = """
DROP PROCEDURE IF EXISTS get_items;
CREATE PROCEDURE get_items(
        IN _id INT
    )
    SELECT * FROM items WHERE wish_id = _id;
"""

GET_ITEM = """
DROP PROCEDURE IF EXISTS get_item;
CREATE PROCEDURE get_item(
        IN _id INT
    )
    SELECT * FROM items WHERE item_id=_id;
"""

DROP_TABLES = """
DROP TABLE IF EXISTS wishes;
DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS items;
"""

commands = [
    DROP_TABLES,

    CREATE_TABLE_USERS,
    CREATE_TABLE_WISHES,
    CREATE_TABLE_ITEMS,

    INSERT_ITEM,
    GET_ITEMS,
    GET_ITEM,


    INSERT_USER,
    GET_USERS,
    GET_USER,
    DELETE_USER,
    UPDATE_USER,

    INSERT_WISH,
    GET_WISHES,
    DELETE_WISH,
    DELETE_WISH_ITEM,
    UPDATE_ITEM,
]

dbConnection = pymysql.connect(
    settings.DB_HOST,
    settings.DB_USER,
    settings.DB_PASSWD,
    settings.DB_DATABASE,
    charset='utf8mb4',
    cursorclass=pymysql.cursors.DictCursor)
cursor = dbConnection.cursor()
for command in commands:
    cursor.execute(command)
dbConnection.commit()                              