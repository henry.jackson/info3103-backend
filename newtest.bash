#!/usr/bin/env bash


BASE_URL="https://info3103.cs.unb.ca:8020"
COOKIE="-b cookie-jar"


# Create a wish after sign in
# curl -k $COOKIE -H "Content-Type: application/json" -d '{"wish_name" :"Birthday"}' -X POST "$BASE_URL/users/1/wishes"

# Delete a wish
# curl -k $COOKIE -X DELETE "$BASE_URL/users/1/wishes/1"

# Add item to the wish
# curl -k -H "Content-Type: application/json" -d '{"name":"lamp", "description":"makes your room shiny"}' -X POST "$BASE_URL/users/1/wishes/1/items"

# List the items in the wish list

curl -k "$BASE_URL/users/1/wishes/1/items"