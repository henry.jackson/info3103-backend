#!/usr/bin/env bash

echo "clear and setup database"

python3 db-setup.py

BASE_URL="https://info3103.cs.unb.ca:8039"
COOKIE="-b cookie-jar"

echo "get all items: should be empty"
curl -k "$BASE_URL/items"
echo "create an item"
curl -k -H "Content-Type: application/json" -d '{"name":"lamp", "description":"makes your room shiny"}' -X POST "$BASE_URL/items"
curl -k -H "Content-Type: application/json" -d '{"name":"towel", "description":"dries stuff"}' -X POST "$BASE_URL/items"

echo "get item 1"
curl -k "$BASE_URL/items/1"
echo "update item 1"
curl -k -H "Content-Type: application/json" -d '{"name":"lamp", "description": "make all places very shiny"}' -X PUT  "$BASE_URL/items/1"
echo "get updated item"
curl -k "$BASE_URL/items/1"
#echo "delete item 1"
#curl -k -X DELETE "$BASE_URL/items/1"

echo "create user"
curl -k -H "Content-Type: application/json" -d '{"username" :"saskari"}' -X POST "$BASE_URL/users"
echo "get all users"
curl -k "$BASE_URL/users"

echo "get user 1"
curl -k "$BASE_URL/users/1"
#echo "update user 1"
#curl -k -H "Content-Type: application/json" -d '{"username": "ali"}' -X PUT  "$BASE_URL/users/1"
#echo "delete user 1"
#curl -k -X DELETE "$BASE_URL/users/1"

# wishes
echo "add wish but not logined so we should get an error"
curl -k -H "Content-Type: application/json" -d '{"item_id" :"1"}' -X POST "$BASE_URL/users/1/wishes"
echo "add wish but with cookie"
curl -k $COOKIE -H "Content-Type: application/json" -d '{"item_id" :"1"}' -X POST "$BASE_URL/users/1/wishes"
echo "get all wishes for user 1"
curl -k "$BASE_URL/users/1/wishes"
echo "delete wish 1 from user1"
curl -k $COOKIE -X DELETE "$BASE_URL/users/1/wishes/1"

# errors
echo "create duplicated user (we should get an error)"
curl -k -H "Content-Type: application/json" -d '{"username" :"saskari"}' -X POST "$BASE_URL/users"
echo "create user without username (we should get an error)"
curl -k -H "Content-Type: application/json" -d '{"fdsfd" :"saskari"}' -X POST "$BASE_URL/users"
echo "create bad item (we should get an error)"
curl -k -H "Content-Type: application/json" -d '{"fsaname":"lamp", "description":"makes your room shiny"}' -X POST "$BASE_URL/items"
echo "add bad wish (we should get an error)"
curl -k $COOKIE -H "Content-Type: application/json" -d '{"iteid" :"1"}' -X POST "$BASE_URL/users/1/wishes"
echo "get non existent item (should get an error)"
curl -k "$BASE_URL/items/10"
